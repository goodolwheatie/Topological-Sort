#include "Graph.h"

ostream& operator<<(ostream& out, const AdjacencyMatrixGraph& g)
{
	for (int i = 0; i < g.numberOfVertices; ++i)
	{
		out << g.vertices[i] << ": ";
		for (int j = 0; j < g.numberOfVertices; ++j)
		{
			out << g.matrix[i][j] << " ";
		}
		out << endl;
	}
	return out;
}


AdjacencyMatrixGraph::AdjacencyMatrixGraph()
{
	numberOfVertices = 0;
	int maxVertices = MAX_VERTICES;
	vertices = new char[maxVertices];
	matrix = new int*[maxVertices];

	for (int i = 0; i < maxVertices; ++i)
	{
		matrix[i] = new int[maxVertices]();
	}

}

AdjacencyMatrixGraph::AdjacencyMatrixGraph(int totalVertices)
{
	numberOfVertices = 0;
	int maxVertices = totalVertices;
	vertices = new char[maxVertices];
	matrix = new int*[maxVertices];

	for (int i = 0; i < maxVertices; ++i)
	{
		matrix[i] = new int[maxVertices]();
	}
}

void AdjacencyMatrixGraph::createGraph(const string & fileName)
{
	ifstream inFile(fileName);

	if (!inFile)
	{
		cerr << "Cannot open input file." << endl;
		system("pause");
		exit(0);
	}

	inFile >> numberOfVertices;

	for (int i = 0; i < numberOfVertices; ++i)
	{
		inFile >> vertices[i];
	}
	
	for (int i = 0; i < numberOfVertices; ++i)
	{
		for (int j = 0; j < numberOfVertices; ++j)
		{
			inFile >> matrix[i][j];
		}
	}
	inFile.close();
}

void AdjacencyMatrixGraph::topologicalSort(char startVertex, list<char>& order)
{
	int start = 0;
	bool found = false;
	while (!found && start < numberOfVertices)
	{
		if (vertices[start] == startVertex)
		{
			found = true;
		}
		else
		{
			++start;
		}
	}

	bool* visited = new bool[numberOfVertices]();
	
	DFS(start, order, visited);

	delete[] visited;
	visited = nullptr;
}

void AdjacencyMatrixGraph::DFS(int vertex,
	list<char>& order, bool* visited)
{
	visited[vertex] = true;
	for (int j = 0; j < numberOfVertices; ++j)
	{
		if (matrix[vertex][j] == 1 && !visited[j])
		{
			DFS(j, order, visited);
		}
	}

	order.push_front(vertices[vertex]);
}
