#include "Graph.h"
#include <iostream>


int main()
{
	AdjacencyMatrixGraph xD;

	xD.createGraph("AdjacencyMatrix.txt");

	cout << xD << endl;
	
	list<char> order;

	xD.topologicalSort('A', order);

	for (char vertex : order)
	{
		cout << vertex << " ";
	}

	cout << endl;
	system("pause");
	return 0;
}