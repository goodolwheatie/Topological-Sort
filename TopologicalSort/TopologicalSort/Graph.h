
#ifndef GRAPH_H
#define GRAPH_H

#include <fstream>
#include <iostream>
#include <list>

using namespace std;

const int MAX_VERTICES = 20;
// Adjacency Matrix
class AdjacencyMatrixGraph
{
	friend ostream& operator<<(ostream&, const AdjacencyMatrixGraph&);

public:

	AdjacencyMatrixGraph();
	AdjacencyMatrixGraph(int);

	void createGraph(const string&);

	void topologicalSort(char, list<char>&);

private:

	char *vertices;
	int **matrix;
	void DFS(int, list<char>&, bool*);

	int numberOfVertices;
};



#endif
